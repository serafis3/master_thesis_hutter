import matplotlib.pyplot as plt
import numpy as np
import xarray as xr

path = "/data/KENDA/SAMOS/SAMOS_Results.nc"

data = xr.open_dataset(path)
print(data)

def plot(station, index, length, name = "Patscherkofel", save_dir = False):
    """This function creates a basic plot, that shows observations, and first guess values for one individual station. 
    Parameters: 
    station: INT, station number, e.g. 11126 for Patschkofel
    index: INT, The index of the timestep, starting from 2015-11-13 with hourly steps
    length: INT, The number of hours you want to plot, e.g. 24 - plot one day, 48 - plot two days
    name: STR, station name, e.g. 'Patscherkofel'
    save_dir: (False | str) if you want to save the figure, set save_dir = 'path/to/plot.png'
    """
    t = data["time"].values
    Obs = data.sel(statid=station).Obs.values
    E_O = data.sel(statid=station).Obs_error.values
    FG_mean = data.sel(statid=station).FG_mean.values
    FG_std = data.sel(statid=station).FG_std.values
    FG_corr = data.sel(statid=station).FG_mean_corr.values
    SD_corr = data.sel(statid=station).FG_std_corr.values
    ti = t[index:index+length]

    plt.figure(figsize=(16,6))
    plt.title(name, fontsize=20)
    plt.plot(ti, Obs[index:index+length]-273.15, color='black', label= r'$o$')
    plt.plot(ti, FG_corr[index:index+length]-273.15, color='blue', label = r'$m_{corr}$')
    plt.fill_between(ti, FG_corr[index:index+length]-273.15, FG_corr[index:index+length]+SD_corr[index:index+length]-273.15, color='#539ecd', alpha = 0.5, label=r"$m_{corr} \pm s_{corr}$")
    plt.fill_between(ti, FG_corr[index:index+length]-273.15, FG_corr[index:index+length]-SD_corr[index:index+length]-273.15, color='#539ecd', alpha = 0.5)
    plt.plot(ti, FG_mean[index:index+length]-273.15, color='red', label = r'$m$')
    plt.fill_between(ti, FG_mean[index:index+length]-273.15, FG_mean[index:index+length]+FG_std[index:index+length]-273.15, color='salmon', alpha = 0.5, label=r"$m \pm s$")
    plt.fill_between(ti, FG_mean[index:index+length]-273.15, FG_mean[index:index+length]-FG_std[index:index+length]-273.15, color='salmon', alpha = 0.5)
    plt.xlabel('Days', fontsize=20)
    plt.ylabel('Temperature [°C]', fontsize=20)
    plt.legend(fontsize=18)
    plt.xticks(fontsize = 18)
    plt.yticks(fontsize = 18)
    plt.grid()
    plt.show()
    if save_dir:
        plt.savefig(save_dir)

plot(11126, 18720, 120, "Patscherkofel")
